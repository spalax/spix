Welcome to SpiX's documentation!
================================

SpiX is `yet another compilation tool <https://www.ctan.org/topic/compilation>`__ for ``.tex`` files. It aims at being simple and human readable. Every piece of configuration is written in the ``.tex`` file itself, in a clear format (a list of console commands).

:ref:`quickstart` should give you enough information to start using SpiX.
License and links are given in :ref:`support`.
In :ref:`why`, you can find out if you should use SpiX, or if you should prefer another tool.
To install SpiX, read :ref:`install`.
The detailed SpiX manual is in :ref:`usage`.
At last, :ref:`faq` give answers to some questions you might have.

.. toctree::

   quickstart
   support
   why
   install
   usage
   faq
