.. _support:

Support
=======

License
-------

SpiX is licensed under the `Gnu GPL 3 license <https://www.gnu.org/licenses/gpl-3.0.html>`_, or any later version.

Documentation
-------------

* The source documentation is written in ``rst`` format, and compiled using `Sphinx <https://www.sphinx-doc.org>`_. It can be found in `the git repository of this project <https://framagit.org/spalax/spix>`_.
* Those ``.rst`` files are compiled by `Sphinx` into a ``.tex`` file, itself compiled into a ``.pdf`` file. This ``.pdf`` file can be found at `ReadTheDocs.org <https://spix.readthedocs.io/_/downloads/en/latest/pdf/>`_.
* To compile the documentation yourself:

  * `Download and install Sphinx <https://www.sphinx-doc.org/en/master/usage/installation.html>`_.
  * `Download SpiX <https://spix.readthedocs.io/en/latest/install/>`_.
  * Go to the ``doc`` directory.
  * Run ``make html`` or ``make latexpdf``.

Help!
-----

- The home page of SpiX is: http://framagit.org/spalax/spix.
- Documentation is at: http://spix.rtfd.io.
- To report bugs, or ask for help, visit: https://framagit.org/spalax/spix/issues (if you don'f feel like creating yet another account, you can send me an email at ``spalax(at)gresille(dot)org``).
